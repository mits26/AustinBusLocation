import logging
from datetime import datetime

from google.transit.gtfs_realtime_pb2 import VehiclePosition
from pytz import timezone
from typing import List

import gtfs_rt_service
import gtfs_service
from models import Stops
from utils import parse_time

logger = logging.getLogger('resolver')


def resolve_running_trips(query, info, filter_input=None):
    if filter_input is None:
        filter_input = {}
    route_name_prefix = filter_input['name_prefix'] if 'name_prefix' in filter_input else None

    trip_ids = gtfs_rt_service.get_real_time_vehicle_trip_ids()

    trips_with_distinct_headsign = gtfs_service.get_trips_with_distinct_headsign(
        trip_ids, route_name_prefix)

    trip_info_list = [{
        'trip_id': trip.trip_id,
        'route_id': trip.route_id,
        'direction': trip.direction_id,
        'name': trip.trip_headsign,
        'color': trip.routes.route_color
    } for trip in trips_with_distinct_headsign]

    # Alphabetically sort trips by route_id
    trip_info_list.sort(key=lambda trip_info: int(trip_info['route_id']))
    return trip_info_list


def resolve_stops(query, info, trip_id):
    return gtfs_service.get_stops_by_trip_id(trip_id)


def resolve_stop(query, info, stop_id) -> Stops:
    return gtfs_service.get_stop(stop_id)


def resolve_route_shapes(query, info, trip_id):
    return gtfs_service.get_shapes_by_trip_id(trip_id)


def resolve_vehicle_positions(query, info, route_id: int, direction):
    return gtfs_rt_service.get_real_time_vehicle_positions(str(route_id), direction)


def resolve_arrival_times(query, info, route_id, direction, stop_id):
    """Finds the arrival times of a route at a given stop.

    Args:
      route_id: The route_id to find arrival times for.
      stop_id: The bus stop.

    Returns:
      A json object with fields
        'arrival_times': An array of arrival info, each item is an object:
          'vehicle_info': Vehicle information.
          'arrival_time': The time the vehicle arrives at the given stop
          :param route_id:
          :param stop_id:
          :param direction:
    """

    # TODO: load arrival time for scheduled trips (non-running trips)

    vehicles = gtfs_rt_service.get_real_time_vehicle_positions(
        route_id, direction)
    vehicle_by_trip_id = {
        gtfs_rt_service.get_trip_id(v): v for v in vehicles
    }

    remove_past_vehicles(vehicle_by_trip_id, stop_id)

    arrival_time_by_trip_id = {}
    trip_id_set = set(vehicle_by_trip_id.keys())

    trip_updates = gtfs_rt_service.get_real_time_trip_updates(
        list(trip_id_set))

    [populate_scheduled_arrival_time(arrival_time_by_trip_id, stop_id, trip_id) for trip_id in trip_id_set]

    [populate_updated_arrival_time(arrival_time_by_trip_id[trip_update.trip.trip_id], stop_id, trip_update)
     for trip_update in trip_updates]

    arrival_times = [{
        'vehicle': vehicle_position,
        'scheduled_arrival_time': arrival_time_by_trip_id[trip_id]['scheduled_arrival_time'],
        'updated_arrival_time': arrival_time_by_trip_id[trip_id]['updated_arrival_time'],
        'trip': gtfs_service.get_trip_by_id(trip_id),
    } for (trip_id, vehicle_position) in vehicle_by_trip_id.items()
        if trip_id in arrival_time_by_trip_id]

    # Sort the arrival times by timestamp
    arrival_times.sort(key=lambda x: x['scheduled_arrival_time'], reverse=False)
    return arrival_times


def remove_past_vehicles(vehicle_by_trip_id, stop_id):
    past_vehicle_trip_ids = []
    for (trip_id, vehicle_position) in vehicle_by_trip_id.items():
        stop_time = gtfs_service.get_stop_time(trip_id, stop_id)
        current_stop_sequence = vehicle_by_trip_id[stop_time.trip_id].current_stop_sequence
        if stop_time.stop_sequence < current_stop_sequence:
            past_vehicle_trip_ids.append(trip_id)

    for trip_id in past_vehicle_trip_ids:
        del vehicle_by_trip_id[trip_id]


def populate_updated_arrival_time(arrival_time, stop_id, trip_update):
    stop_time_update = gtfs_rt_service.get_arrival_time_by_stop_id(
        trip_update.stop_time_update, stop_id)

    if stop_time_update is None:
        arrival_time.update(updated_arrival_time=None, trip_update=None)
        return

    arrival_time_update = stop_time_update.arrival.time if stop_time_update.HasField('arrival') \
        else stop_time_update.departure.time

    updated_arrival_time = datetime.fromtimestamp(arrival_time_update).astimezone(
        timezone('US/Central')).strftime('%H:%M:%S')

    arrival_time.update(
        updated_arrival_time=updated_arrival_time, trip_update=trip_update)


def populate_scheduled_arrival_time(arrival_time_by_trip_id, stop_id, trip_id):
    stop_time = gtfs_service.get_stop_time(trip_id, stop_id)

    arrival_time_by_trip_id[trip_id] = {
        'scheduled_arrival_time': stop_time.arrival_time
    }


def resolve_trip(query, info, trip_id):
    return gtfs_service.get_trip_by_id(trip_id)


# For debugging purposes
def resolve_vehicle_positions_debug(query, info) -> List[VehiclePosition]:
    return gtfs_rt_service.load_vehicle_positions()
