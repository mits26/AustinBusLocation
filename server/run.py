import os
from app import create_app

if __name__ == '__main__':
    # https://stackoverflow.com/questions/17260338/deploying-flask-with-heroku
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    environment = os.environ.get('ENVIRONMENT', 'LOCAL')

    debug = environment == 'LOCAL'
    austin_bus_go_app = create_app()
    austin_bus_go_app.run(host='0.0.0.0', port=port, debug=debug)
