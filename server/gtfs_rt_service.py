from typing import List

import requests
from google.transit.gtfs_realtime_pb2 import VehiclePosition, FeedEntity, TripUpdate
from google.transit import gtfs_realtime_pb2

import gtfs_service

capital_metro_trip_updates_pb_file_url = 'https://data.texas.gov/download/rmk2-acnw/application%2Foctet-stream'
capital_metro_vehicle_positions_pb_file_url = 'https://data.texas.gov/download/eiei-9rpf/application%2Foctet-stream'


def get_real_time_vehicle_trip_ids(route_id: str = None) -> List[str]:
    return [get_trip_id(vehicle_position) for vehicle_position in load_vehicle_positions(route_id=route_id)]


def get_real_time_vehicle_positions(route_id: str, direction) -> List[VehiclePosition]:
    current_vehicle_positions = load_vehicle_positions(route_id=route_id)
    trip_ids = [get_trip_id(vehicle_position)
                for vehicle_position in current_vehicle_positions]

    trips_on_route = gtfs_service.get_trips_with_direction_and_route(
        trip_ids, int(route_id), direction)
    return [vehicle_position for vehicle_position in current_vehicle_positions if get_trip_id(vehicle_position)
            in trips_on_route]


def get_trip_id(vehicle: VehiclePosition) -> str:
    return vehicle.trip.trip_id


def get_real_time_trip_updates(trip_ids: List[str] = None) -> List[TripUpdate]:
    if trip_ids is None:
        return load_trip_updates()
    else:
        return [trip_update_list for trip_update_list
                in load_trip_updates() if trip_update_list.trip.trip_id in trip_ids]


def get_arrival_time_by_stop_id(stop_time_updates: List[TripUpdate.StopTimeUpdate], stop_id: str) \
        -> TripUpdate.StopTimeUpdate or None:
    try:
        return next(stop_time_update for stop_time_update in stop_time_updates
                    if stop_time_update.stop_id == str(stop_id))
    except StopIteration:
        return None


def load_trip_updates() -> List[TripUpdate]:
    response = requests.get(capital_metro_trip_updates_pb_file_url)
    feed_message = gtfs_realtime_pb2.FeedMessage()
    feed_message.ParseFromString(response.content)
    return [feed_entity.trip_update for feed_entity in feed_message.entity]


def load_vehicle_positions(trip_required: bool = True, route_id: str = None) -> List[VehiclePosition]:
    response = requests.get(capital_metro_vehicle_positions_pb_file_url)
    feed_message = gtfs_realtime_pb2.FeedMessage()
    feed_message.ParseFromString(response.content)
    # Convert feed to dict: https://mayors-ic.github.io/examples/gtfs-example.html
    return get_valid_vehicles(feed_message.entity, route_id)


def get_valid_vehicles(feed_entities: List[FeedEntity], route_id: str) -> List[VehiclePosition]:
    """
    Filters out vehicles without a trip.
    :param route_id:
    :param feed_entities: list of vehicle positions to be filtered
    :return: list of vehicle positions with trips defined.
    """
    if route_id is None:
        return [feed_entity.vehicle for feed_entity in feed_entities if feed_entity.vehicle.HasField('trip')]
    else:
        return [feed_entity.vehicle for feed_entity in feed_entities if feed_entity.vehicle.HasField('trip') and
                feed_entity.vehicle.trip.route_id == str(route_id)]
