export PYTHONPATH := ./server

run:
	python3 server/run.py

test:
	python3 -m unittest discover -s tests

coverage:
	coverage run --source=./server -m unittest discover -s tests
	coverage report -m

run-ci:
	gunicorn 'server.app:create_app()'

lint:
	autopep8 --in-place -v --recursive server/.

downloadGTFS:
	./ci-job/downloadGTFS.sh