import unittest

from peewee import DoesNotExist, SqliteDatabase

from gtfs_service import get_stop, get_shapes_by_trip_id
from models import Stops, Trips, Shapes

MODELS = [Stops, Trips, Shapes]

database = SqliteDatabase(':memory:')


class TestCase(unittest.TestCase):
    def setUp(self):
        database.bind(MODELS)
        database.connect()
        database.create_tables(MODELS)

    def tearDown(self):
        database.drop_tables(MODELS)
        database.close()

    def test_trivial(self):
        self.assertEqual(1, 1)

    def test_get_stop(self):
        stop = Stops.create(stop_id=1)

        self.assertEqual(get_stop(1).stop_id, stop.stop_id)

    def test_get_stop_not_found(self):
        with self.assertRaises(DoesNotExist) as context:
            get_stop(2)

        self.assertTrue('instance matching query does not exist' in str(context.exception))

    def test_shape(self):
        trip = Trips.create(trip_id='trip_1', shape_id='1')
        shape = Shapes.create(shape_id=trip.shape_id, trip_id=trip.trip_id)

        self.assertEqual(len(get_shapes_by_trip_id('trip_1')), 1)
        self.assertEqual(get_shapes_by_trip_id('trip_1')[0].shape_id, shape.shape_id)

    def test_shape_not_fount(self):
        with self.assertRaises(DoesNotExist) as context:
            get_shapes_by_trip_id('trip_1')


if __name__ == '__main__':
    unittest.main()
