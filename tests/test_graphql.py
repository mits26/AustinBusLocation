import unittest
from graphene.test import Client
from peewee import SqliteDatabase

from models import Stops, Trips, Shapes
from schema import schema

MODELS = [Stops]

database = SqliteDatabase(':memory:')


class TestCase(unittest.TestCase):
    def setUp(self):
        database.bind(MODELS)
        database.connect()
        database.create_tables(MODELS)

    def tearDown(self):
        database.drop_tables(MODELS)
        database.close()

    def test_stop(self):
        stop = Stops.create(stop_id=2493, stop_code=2493, stop_name="3107 Red River/32nd", stop_lat=30.290665,
                            stop_lon=-97.727112)

        client = Client(schema)
        executed = client.execute('''
            query {
                stop(stopId: "2493") {
                    stopId
                    stopCode
                    stopName
                    stopLat
                    stopLon
                }
            }
        ''')

        self.assertDictEqual(executed['data']['stop'], {
            'stopId': stop.stop_id,
            'stopCode': str(stop.stop_code),
            'stopName': stop.stop_name,
            'stopLat': stop.stop_lat,
            'stopLon': stop.stop_lon
        })


if __name__ == '__main__':
    unittest.main()
