import * as Types from "../interfaces/interface.d";

export type ArrivalTimesQueryVariables = {
  routeId: Types.Scalars["Int"];
  direction: Types.Scalars["Boolean"];
  stopId: Types.Scalars["String"];
};

export type ArrivalTimesQuery = { __typename?: "Query" } & {
  arrivalTimes?: Types.Maybe<
    Array<
      { __typename?: "ArrivalTime" } & Pick<
        Types.ArrivalTime,
        "updatedArrivalTime" | "scheduledArrivalTime"
      > & {
          trip: { __typename?: "Trip" } & Pick<
            Types.Trip,
            | "routeId"
            | "serviceId"
            | "tripId"
            | "tripHeadsign"
            | "tripShortName"
            | "directionId"
            | "blockId"
            | "shapeId"
            | "wheelchairAccessible"
            | "bikesAllowed"
          >;
          vehicle: { __typename?: "VehiclePosition" } & Pick<
            Types.VehiclePosition,
            "stopId" | "currentStatus" | "timestamp"
          > & {
              trip?: Types.Maybe<
                { __typename?: "TripDescriptor" } & Pick<
                  Types.TripDescriptor,
                  "tripId" | "routeId" | "startDate"
                >
              >;
              vehicle?: Types.Maybe<
                { __typename?: "VehicleDescriptor" } & Pick<
                  Types.VehicleDescriptor,
                  "id" | "label"
                >
              >;
              position?: Types.Maybe<
                { __typename?: "Position" } & Pick<
                  Types.Position,
                  "latitude" | "longitude"
                >
              >;
            };
        }
    >
  >;
};
