import * as Types from "../interfaces/interface.d";

export type TripQueryVariables = {
  tripId: Types.Scalars["String"];
};

export type TripQuery = { __typename?: "Query" } & {
  trip: { __typename?: "Trip" } & Pick<
    Types.Trip,
    | "routeId"
    | "serviceId"
    | "tripId"
    | "tripHeadsign"
    | "tripShortName"
    | "directionId"
    | "blockId"
    | "shapeId"
    | "wheelchairAccessible"
    | "bikesAllowed"
  >;
};
