import * as Types from "../interfaces/interface.d";

export type StopQueryVariables = {
  stopId: Types.Scalars["String"];
};

export type StopQuery = { __typename?: "Query" } & {
  stop: { __typename?: "Stop" } & Pick<
    Types.Stop,
    "stopId" | "stopCode" | "stopName" | "stopLat" | "stopLon"
  >;
};
