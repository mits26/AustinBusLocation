import * as Types from "../interfaces/interface.d";

export type RunningTripsQueryVariables = {
  filterInput?: Types.Maybe<Types.RunningTripFilterInput>;
};

export type RunningTripsQuery = { __typename?: "Query" } & {
  runningTrips?: Types.Maybe<
    Array<
      { __typename?: "RunningTrip" } & Pick<
        Types.RunningTrip,
        "name" | "routeId" | "direction" | "color" | "tripId"
      >
    >
  >;
};
