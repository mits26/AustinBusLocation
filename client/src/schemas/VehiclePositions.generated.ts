import * as Types from "../interfaces/interface.d";

export type VehiclePositionsQueryVariables = {
  routeId: Types.Scalars["Int"];
  direction: Types.Scalars["Boolean"];
};

export type VehiclePositionsQuery = { __typename?: "Query" } & {
  vehiclePositions?: Types.Maybe<
    Array<
      { __typename?: "VehiclePosition" } & Pick<
        Types.VehiclePosition,
        "stopId" | "currentStatus" | "timestamp" | "congestionLevel"
      > & {
          trip?: Types.Maybe<
            { __typename?: "TripDescriptor" } & Pick<
              Types.TripDescriptor,
              "tripId" | "routeId" | "startDate" | "startTime"
            >
          >;
          vehicle?: Types.Maybe<
            { __typename?: "VehicleDescriptor" } & Pick<
              Types.VehicleDescriptor,
              "id" | "label" | "licensePlate"
            >
          >;
          position?: Types.Maybe<
            { __typename?: "Position" } & Pick<
              Types.Position,
              "latitude" | "longitude" | "bearing" | "speed"
            >
          >;
        }
    >
  >;
};
