import { gql } from "apollo-boost";

export const RUNNING_TRIPS_QUERY = gql`
  query RunningTrips($filterInput: RunningTripFilterInput) {
    runningTrips(filterInput: $filterInput) {
      name
      routeId
      direction
      color
      tripId
    }
  }
`;
