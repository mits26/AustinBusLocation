import * as Types from "../interfaces/interface.d";

export type StopsQueryVariables = {
  tripId: Types.Scalars["String"];
};

export type StopsQuery = { __typename?: "Query" } & {
  stops?: Types.Maybe<
    Array<
      { __typename?: "Stop" } & Pick<
        Types.Stop,
        "stopId" | "stopCode" | "stopName" | "stopLat" | "stopLon"
      >
    >
  >;
  routeShapes?: Types.Maybe<
    Array<
      { __typename?: "Shape" } & Pick<Types.Shape, "shapePtLat" | "shapePtLon">
    >
  >;
};
