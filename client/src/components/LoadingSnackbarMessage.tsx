import { CircularProgress } from "@material-ui/core";
import * as React from "react";

interface LoadingSnackbarMessageProps {
  readonly message: string;
}

export const LoadingSnackbarMessage: React.FunctionComponent<LoadingSnackbarMessageProps> = (
  props: LoadingSnackbarMessageProps
) => {
  const { message } = props;
  return (
    <React.Fragment>
      {message}
      <CircularProgress
        color="secondary"
        size={24}
        style={{ marginLeft: 16 }}
      />
    </React.Fragment>
  );
};
