import { IconButton } from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";
import Paper from "@material-ui/core/Paper";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import SettingsIcon from "@material-ui/icons/Settings";
import { Autocomplete } from "@material-ui/lab";
import * as React from "react";
import { RunningTrip } from "../interfaces/interface.d";

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginLeft: 25,
      marginTop: 10,
      padding: "2px 10px",
      display: "flex",
      alignItems: "center",
      width: 350,
    },
    autoComplete: {
      width: 300,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
      width: "100%",
    },
    iconButton: {
      marginLeft: 10,
      padding: 10,
    },
  })
);

export interface SearchPanelProps {
  readonly runningTrips: RunningTrip[];
  readonly loading?: boolean;
  readonly trip?: RunningTrip;
  setTrip(trip?: RunningTrip): void;
  openSettingsDialog(): void;
}

export const SearchPanel: React.FunctionComponent<SearchPanelProps> = (
  props: SearchPanelProps
) => {
  const { runningTrips, loading, trip, setTrip, openSettingsDialog } = props;
  const classes = useStyles();

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const searchRouteOnChange = (event: any, newValue: RunningTrip | null) => {
    if (newValue != null) {
      setTrip(newValue);
    } else {
      setTrip(undefined);
    }
  };

  return (
    <Paper className={classes.root}>
      <Autocomplete
        id="combo-box-demo"
        options={runningTrips || []}
        loading={loading}
        className={classes.autoComplete}
        value={trip || null}
        blurOnSelect={true}
        onChange={searchRouteOnChange}
        // groupBy={option => option.color || ""}
        getOptionLabel={option => option.name}
        renderInput={params => (
          <InputBase
            placeholder={"Search Routes"}
            ref={params.InputProps.ref}
            inputProps={params.inputProps}
            className={classes.input}
          />
        )}
      />
      <IconButton
        className={classes.iconButton}
        aria-label="directions"
        onClick={openSettingsDialog}
      >
        <SettingsIcon />
      </IconButton>
    </Paper>
  );
};
