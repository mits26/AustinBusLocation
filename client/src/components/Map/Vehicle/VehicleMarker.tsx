import { Popover } from "@material-ui/core";
import { useState } from "react";
import * as React from "react";
import { Marker } from "react-map-gl";
import { VehiclePosition } from "../../../interfaces/interface.d";
import { VehicleIcon } from "./VehicleIcon";
import { iconSize } from "../Map";
import { VehiclePopupContainer } from "./VehiclePopupContainer";

interface VehicleMarkerProps {
  readonly vehiclePosition: VehiclePosition;
  readonly onClick: (vehiclePosition: VehiclePosition) => void;
}

export const VehicleMarker: React.FunctionComponent<VehicleMarkerProps> = (
  props: VehicleMarkerProps
) => {
  const { vehiclePosition } = props;
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const onClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
    props.onClick(vehiclePosition);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const { position } = vehiclePosition;
  return (
    <React.Fragment>
      <Marker
        longitude={position?.longitude || 0}
        latitude={position?.latitude || 0}
        key={vehiclePosition?.vehicle?.id || ""}
        offsetLeft={-iconSize.width / 2}
        offsetTop={-iconSize.height}
      >
        <VehicleIcon bearing={position?.bearing || 0} onClick={onClick} />
      </Marker>
      <Popover
        open={open}
        onClose={handleClose}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
      >
        <VehiclePopupContainer vehiclePosition={vehiclePosition} />
      </Popover>
    </React.Fragment>
  );
};
