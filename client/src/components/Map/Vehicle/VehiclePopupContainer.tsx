import { useQuery } from "@apollo/react-hooks";
import React from "react";
import { VehiclePosition } from "../../../interfaces/interface.d";
import { STOP_QUERY } from "../../../schemas/Stop";
import { StopQuery, StopQueryVariables } from "../../../schemas/Stop.generated";
import { TRIP_QUERY } from "../../../schemas/Trip";
import { TripQuery, TripQueryVariables } from "../../../schemas/Trip.generated";
import { VehiclePopupContent } from "./VehiclePopupContent";

export interface VehiclePopupContainerProps {
  readonly vehiclePosition: VehiclePosition;
}

export const VehiclePopupContainer: React.FunctionComponent<VehiclePopupContainerProps> = (
  props: VehiclePopupContainerProps
) => {
  const { vehiclePosition } = props;

  const { data: stop, loading: stopLoading } = useQuery<
    StopQuery,
    StopQueryVariables
  >(STOP_QUERY, {
    variables: {
      stopId: vehiclePosition.stopId || "",
    },
  });

  const { data: trip, loading: tripLoading } = useQuery<
    TripQuery,
    TripQueryVariables
  >(TRIP_QUERY, {
    variables: {
      tripId: vehiclePosition?.trip?.tripId || "",
    },
  });

  return (
    <VehiclePopupContent
      vehiclePosition={vehiclePosition}
      stop={stop}
      stopLoading={stopLoading}
      trip={trip}
      tripLoading={tripLoading}
    />
  );
};
