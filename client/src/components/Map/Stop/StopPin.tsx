import { IconButton, Tooltip } from "@material-ui/core";
import LocationIcon from "@material-ui/icons/LocationOn";
import React from "react";

export type Colors =
  | "inherit"
  | "primary"
  | "secondary"
  | "action"
  | "disabled"
  | "error";

interface StopPinProps {
  readonly stopName: string;
  readonly color?: Colors;
  onClick?(): void;
}

export const StopPin: React.FunctionComponent<StopPinProps> = ({
  onClick,
  color,
  stopName,
}: StopPinProps) => (
  <Tooltip title={stopName}>
    <IconButton onClick={onClick} size={"small"}>
      <LocationIcon fontSize={"small"} color={color} />
    </IconButton>
  </Tooltip>
);
