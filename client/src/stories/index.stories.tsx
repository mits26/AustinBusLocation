import { action } from "@storybook/addon-actions";
import { Button } from "@storybook/react/demo";
import React from "react";
import { ArrivalTimeList } from "../components/ArrivalTimeList";
import { ArrivalTimeListItem } from "../components/ArrivalTimeListItem";
import { Colors, StopPin } from "../components/Map/Stop/StopPin";
import { VehicleIcon } from "../components/Map/Vehicle/VehicleIcon";
import { VehiclePopupContent } from "../components/Map/Vehicle/VehiclePopupContent";
import { SearchPanel } from "../components/SearchPanel";
import { SettingsDialog } from "../components/SettingsDialog";
import { ArrivalTime, VehicleStopStatus } from "../interfaces/interface.d";
import { arrivalTimes, runningTrips, vehiclePosition } from "./mockData";
import {
  withKnobs,
  boolean,
  number,
  radios,
  date,
} from "@storybook/addon-knobs";

export default {
  title: "Index",
  component: Button,
  decorators: [withKnobs],
};

export const vehicleIcon = () => (
  <VehicleIcon
    bearing={number("Bearing", vehiclePosition.position?.bearing || 0)}
    onClick={action("onClick")}
  />
);

export const arrivalTimeList = () => (
  <ArrivalTimeList
    arrivalTimes={arrivalTimes}
    arrivalTimeOnClick={action("Arrival Time Onclick")}
    loading={boolean("Loading", false)}
  />
);

const myDateKnob = (name: string, defaultValue: Date): Date => {
  return new Date(date(name, defaultValue));
};

export const arrivalTimeListItem = () => {
  const arrivalTime: ArrivalTime = {
    updatedArrivalTime: myDateKnob(
      "Updated Arrival Time",
      new Date("04 Dec 1995 00:12:00 CST")
    ).toTimeString(),
    scheduledArrivalTime: myDateKnob(
      "Scheduled Arrival Time",
      new Date("04 Dec 1995 00:13:00 CST")
    ).toTimeString(),
    trip: {
      routeId: "1",
      serviceId: "5-133_MRG_2",
      tripId: "2277200_MRG_2",
      tripHeadsign: "1-Lamar/South Congress SB",
      tripShortName: "William Cannon",
      directionId: false,
      blockId: "1008-5-133_MRG_2",
      shapeId: "42692",
      wheelchairAccessible: 1,
      bikesAllowed: 1,
      __typename: "Trip",
    },
    vehicle: {
      trip: {
        tripId: "2277200_MRG_2",
        routeId: "1",
        startDate: "20200403",
        __typename: "TripDescriptor",
      },
      vehicle: {
        id: "2253",
        label: "2253",
        __typename: "VehicleDescriptor",
      },
      position: {
        latitude: 30.268808364868164,
        longitude: -97.72804260253906,
        __typename: "Position",
      },
      stopId: "3731",
      currentStatus: VehicleStopStatus.StoppedAt,
      timestamp: 1585954951,
      __typename: "VehiclePosition",
    },
  };

  return (
    <ArrivalTimeListItem
      arrivalTime={arrivalTime}
      arrivalTimeOnClick={action("Arrival Time Onclick")}
    />
  );
};

export const arrivalTimeListItemNoUpdate = () => {
  const arrivalTime: ArrivalTime = {
    updatedArrivalTime: "",
    scheduledArrivalTime: myDateKnob(
      "Scheduled Arrival Time",
      new Date("04 Dec 1995 00:13:00 CST")
    ).toTimeString(),
    trip: {
      routeId: "1",
      serviceId: "5-133_MRG_2",
      tripId: "2277200_MRG_2",
      tripHeadsign: "1-Lamar/South Congress SB",
      tripShortName: "William Cannon",
      directionId: false,
      blockId: "1008-5-133_MRG_2",
      shapeId: "42692",
      wheelchairAccessible: 1,
      bikesAllowed: 1,
      __typename: "Trip",
    },
    vehicle: {
      trip: {
        tripId: "2277200_MRG_2",
        routeId: "1",
        startDate: "20200403",
        __typename: "TripDescriptor",
      },
      vehicle: {
        id: "2253",
        label: "2253",
        __typename: "VehicleDescriptor",
      },
      position: {
        latitude: 30.268808364868164,
        longitude: -97.72804260253906,
        __typename: "Position",
      },
      stopId: "3731",
      currentStatus: VehicleStopStatus.StoppedAt,
      timestamp: 1585954951,
      __typename: "VehiclePosition",
    },
  };

  return (
    <ArrivalTimeListItem
      arrivalTime={arrivalTime}
      arrivalTimeOnClick={action("Arrival Time Onclick")}
    />
  );
};

export const searchPanel = () => {
  return (
    <SearchPanel
      runningTrips={runningTrips}
      setTrip={action("Set Trip")}
      trip={undefined}
      loading={boolean("Loading", false)}
      openSettingsDialog={action("Opens Settings Dialog")}
    />
  );
};

export const settingsDialog = () => {
  return (
    <SettingsDialog
      open={true}
      autoPolling={false}
      reloadVehiclePositions={action("Reload vehicle positions")}
      setOpen={action("Set open")}
      setAutoPolling={action("Set auto polling")}
    />
  );
};

export const stopPin = () => {
  const label = "Color";
  const options = {
    primary: "primary",
    secondary: "secondary",
  };
  const defaultValue = "primary";

  const value = radios(label, options, defaultValue);

  return (
    <StopPin
      stopName={"Name"}
      color={value as Colors}
      onClick={action("onClick")}
    />
  );
};

export const vehiclePopupContainer = () => {
  return (
    <VehiclePopupContent
      vehiclePosition={vehiclePosition}
      stop={{
        stop: {
          stopId: 468,
          stopLat: 30.353139,
          stopLon: -97.706082,
          stopCode: "468",
          stopName: "Lamar/Thurmond",
        },
      }}
      stopLoading={boolean("Stop Loading", false)}
      trip={{
        trip: {
          tripId: "2277200_MRG_6",
          tripHeadsign: "1-Lamar/South Congress SB",
          tripShortName: "William Cannon",
          routeId: "1",
          serviceId: "5-133_MRG_6",
          shapeId: "42692",
          wheelchairAccessible: 1,
          bikesAllowed: 1,
        },
      }}
      tripLoading={boolean("Trip Loading", false)}
    />
  );
};
